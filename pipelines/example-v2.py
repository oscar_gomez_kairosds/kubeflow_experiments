# Descripción
"""
Un ejemplo de uso de kubeflow pipelines con el entrenamiento
de un modelo de regresión logística con el dataset iris
"""

# Metadatos
__author__ = "oscar_gomez_kairosds"
__email__ = "oscar.gomez@kairosds.com"
__date__ = "2022-03-15"
__modiified__ = "2022-04-12"
__project__ = "kubernetes_learning"

# Librerías
## Estándard
import argparse
from requests import Session
from typing import NamedTuple
## Paquetes
### Kubeflow Pipelines
import kfp
from kfp import dsl, components
from kfp.v2.dsl import (
    component,
    Input,
    Output,
    ClassificationMetrics,
    Dataset,
    Metrics,
    Model
)

# Defaults de la pipeline
MODEL_NAME = "logreg_iris"
TEST_SIZE = 0.1

# Cargar el dataset iris
@component(packages_to_install=["pandas", "sklearn"])
def load_datasets(
        test_size: float,
        features_train: Output[Dataset],
        features_test: Output[Dataset],
        targets_train: Output[Dataset],
        targets_test: Output[Dataset]
    ):
    ## Doc
    """
    Descripción
    -----------
    Carga y fracciona el dataset Iris en features y targets
    para entrenamiento y evaluación.
    
    Parámetros
    ----------
    test_size: float
        Proporción del dataset Iris usada para evaluación.

    Outputs
    -------
    features_train: Dataset
        Fracción de features del dataset Iris
        destinada a entrenar un modelo.
    targets_train: Dataset
        Fracción de targets del dataset Iris
        destinada a entrenar un modelo.
    features_test: Dataset
        Fracción de features del dataset Iris
        destinada a evaluar un modelo.
    targets_test: Dataset
        Fracción de targets del dataset Iris
        destinada a evaluar un modelo.
    
    Dependencias
    ------------
    pandas
    scitkit-learn
    """
    ## Cache de variables de entrada
    outputs = list(locals().values())[1:]
    ## Librerías
    ### Paquetes
    ### Scikit-learn
    from sklearn.datasets import load_iris
    from sklearn.model_selection import train_test_split
    ## Cargando dataset Iris
    features, targets = load_iris(return_X_y=True, as_frame=True)
    ## Fraccionando dataset
    split_dfs = train_test_split(
        features,
        targets,
        test_size = test_size
    )
    ## Guardando todas las fracciones del dataset
    save_output = lambda df, output: df.to_csv(output.path, index=False)
    [save_output(df, output) for df, output in zip(split_dfs, outputs)]

# Crear y entrenar un modelo
@component(packages_to_install=["pandas","sklearn"])
def create_train_model(
        features_train: Input[Dataset],
        targets_train: Input[Dataset],
        model: Output[Model],
    ):
    ## Doc
    """
    Description
    -----------
    Entrena un modelo de regresión logística
    con una fracción del dataset Iris. Serializa
    el modelo para usarlo en otras tareas.

    Inputs
    ------
    features_train: Dataset
        Fracción de features del dataset Iris
        destinada a entrenar un modelo.
    targets_train: Dataset
        Fracción de targets del dataset Iris
        destinada a entrenar un modelo.
    
    Outputs
    -------
    model: Model
        Modelo de regresión logística entrenado
        con el dataset Iris.
    
    Dependencias
    ------------
    pandas
    scikit-learn
    """
    ## Librerías
    ### Estándard
    from collections import namedtuple
    import pickle
    ### Paquetes
    #### Pandas
    import pandas as pd
    #### Scikit-learn
    from sklearn.linear_model import LogisticRegression
    ## Cargando features y targets de entrenamiento
    features_train_df = pd.read_csv(features_train.path)
    targets_train_df = pd.read_csv(targets_train.path)
    ## Creando y entrenando modelo
    logreg_model = LogisticRegression(random_state=0)\
        .fit(features_train_df, targets_train_df)
    ## Serializando y guardando modelo
    pickle.dump(logreg_model, open(model.path, "wb"))

# Evaluar un modelo
@component(packages_to_install=["numpy", "pandas", "sklearn"])
def evaluate_model(
        features_test: Input[Dataset],
        targets_test: Input[Dataset],
        model: Input[Model],
        scalar_metrics: Output[Metrics],
        special_metrics: Output[ClassificationMetrics]
    ):
    ## Doc
    """
    Descripción
    -----------
    Evalua un modelo de regresión logística
    con una fracción del dataset Iris. Genera
    una métrica de precisión y una matriz de
    confusión.
    
    Inputs
    ------
    features_test: Dataset
        Fracción de features del dataset Iris
        destinada a evaluar un modelo.
    targets_test: Dataset
        Fracción de targets del dataset Iris
        destinada a evaluar un modelo.
    model: Model
        Modelo de regresión logística entrenado
        con el dataset Iris
    
    Ouputs
    ------
    scalar_metrics:
        Métricas que se pueden expresar en
        forma de escalar. Contiene la precisión
        de las prediciones de model sobre features_test
        en comparación con targets_test.
    special_metrics: ClassificationMetrics
        Métricas que no se pueden expresar en
        forma de escalar. Contiene una matriz
        de confusión generada al comparar las
        prediciones de model sobre features_test
        con targets_test.
    
    Dependencias
    ------------
    numpy
    pandas
    scikit-learn
    """
    ## Librerías
    ### Estándard
    import pickle
    ### Paquetes
    #### Numpy
    import numpy as np
    #### Pandas
    import pandas as pd
    #### Scikit-learn
    from sklearn.metrics import confusion_matrix
    ## Cargando features y targets de evaluación
    features_test_df = pd.read_csv(features_test.path)
    targets_test_df = pd.read_csv(targets_test.path)
    ## Cargando modelo
    logreg_model = pickle.load(open(model.path,"rb"))
    ## Generando predicciones
    model_predictions = logreg_model.predict(features_test_df)
    ## Registrando métricas escalares
    model_predictions = np.array(model_predictions)
    targets_test_df = targets_test_df.to_numpy().flatten()
    scalar_metrics.log_metric(
        "accuracy",
        100*(model_predictions == targets_test_df).mean()
    )
    ## Registrando métricas especiales
    special_metrics.log_confusion_matrix(
        ["Setosa", "Versicolour", "Virginica"],
        confusion_matrix(targets_test_df, model_predictions).tolist()
    )

# Pipeline definition
@dsl.pipeline(
    name="experimento_kfn_kfp",
    description="""
    Flujo de trabajo para entrenar 
    y desplegar un modelo sencillo
    """
)
def pipeline_main(
        test_size: float,
        model_name: str,
        namespace: str,
        kserve_version: str
    ):
    ## Loading datasets
    datasets = load_datasets(test_size=test_size)
    ## Creating and trainning model
    logreg_model = create_train_model(
        features_train=datasets.outputs["features_train"],
        targets_train=datasets.outputs["targets_train"],
    )
    ## Evaluating model
    evaluate_model(
        features_test=datasets.outputs["features_test"],
        targets_test=datasets.outputs["targets_test"],
        model=logreg_model.outputs["model"]
    )

# Session de kfp (source https://github.com/kubeflow/kfctl/issues/140)
## Configuración
HOST = "host"
USERNAME = "usuario"
PASSWORD = "contraseña"
NAMESPACE = "kubeflow"
## Iniciamos sesion
session = Session()
response = session.get(HOST)
headers = {
    "Content-Type": "application/x-www-form-urlencoded",
}
## Registramos cookies
data = {"login": USERNAME, "password": PASSWORD}
session.post(response.url, headers=headers, data=data)
session_cookie = session.cookies.get_dict()["authservice_session"]

client = kfp.Client(
    host=f"{HOST}/pipeline",
    cookies=f"authservice_session={session_cookie}",
    namespace=NAMESPACE,
)
client.list_experiments()
client.create_run_from_pipeline_func(
    pipeline_main,
    arguments={
        "test_size": TEST_SIZE,
        "model_name": MODEL_NAME
    },
    experiment_name="pruebas",
    mode=kfp.dsl.PipelineExecutionMode.V2_COMPATIBLE
)